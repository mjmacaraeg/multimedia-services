package com.springboot.example.repository;

import com.springboot.example.entity.Speaker;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpeakerRepository extends CrudRepository<Speaker, Integer> {

}
