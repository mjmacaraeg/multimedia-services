package com.springboot.example.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "Skill")
@Getter
@ToString
@NoArgsConstructor
public class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "skill_sequence")
    /*    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auto_gen") */
    /*    @SequenceGenerator(name = "auto_gen", sequenceName = "skill_sequenceA") */
    @Column(name = "skill_id")
    private int skill_id;

    @NotEmpty
    @Column(name = "skill_name")
    private String skill_name;

    @NotEmpty
    @Column(name = "skill_desc")
    private String skill_desc;

    @Column(name = "skill_level")
    private Integer skill_level;

    public Skill(String skill_name, String skill_desc, int skill_level) {
        this.skill_name = skill_name;
        this.skill_desc = skill_desc;
        this.skill_level = skill_level;
    }

}
