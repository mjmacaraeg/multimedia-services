package com.springboot.example.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Getter
@ToString
@NoArgsConstructor
public class Speaker {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "member_sequence")
    @Column(name = "speaker_id")
    private Integer id;

    @NotEmpty
    @Column(name = "speaker_name")
    private String name;

    @NotEmpty
    @Column(name = "speaker_email")
    private String email;

    @NotEmpty
    @Column(name = "speaker_mobile")
    private String mobile;

    @ManyToMany(mappedBy = "speakers")
    @JsonIgnore
    private List<Session> sessions;

    public Speaker(String name, String email, String mobile) {
        this.name = name;
        this.email = email;
        this.mobile = mobile;
    }

}
