package com.springboot.example.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Session")
@Getter
@ToString
@NoArgsConstructor
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "session_sequence")
    @Column(name = "session_id")
    private int session_id;

    @NotEmpty
    @Column(name = "session_name")
    private String session_name;

    @NotEmpty
    @Column(name = "session_desc")
    private String session_desc;

    @Column(name = "session_date")
    private Date session_date;

    @ManyToMany
    @JoinTable(
            name = "session_speaker",
            joinColumns = @JoinColumn(name = "session_id"),
            inverseJoinColumns = @JoinColumn(name = "speaker_id"))
    private List<Speaker> speakers;

    public Session(String session_name, String session_desc, Date session_date) {
        this.session_name = session_name;
        this.session_desc = session_desc;
        this.session_date = session_date;
    }

}
