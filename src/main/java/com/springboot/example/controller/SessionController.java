package com.springboot.example.controller;


import com.springboot.example.entity.Session;
import com.springboot.example.repository.SessionRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class SessionController {
    private final SessionRepository sessionRepository;

    public SessionController(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @GetMapping(value = "/session", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Session> getSessions() {
        return sessionRepository.findAll();
    }

    @GetMapping(value = "session/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Session getSession(@PathVariable int id) {
        return sessionRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid session id %s", id)));
    }

    @PostMapping(value = "/session", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Session postSession(@Valid @RequestBody Session session) {
        return sessionRepository.save(session);
    }

}