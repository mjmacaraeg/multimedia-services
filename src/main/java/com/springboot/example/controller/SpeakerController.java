package com.springboot.example.controller;


import com.springboot.example.entity.Speaker;
import com.springboot.example.repository.SpeakerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class SpeakerController {
    private final SpeakerRepository speakerRepository;

    public SpeakerController(SpeakerRepository speakerRepository) {
        this.speakerRepository = speakerRepository;
    }

    @GetMapping(value = "/speaker", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Speaker> getSpeakers() {
        return speakerRepository.findAll();
    }

    @GetMapping(value = "speaker/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Speaker getSpeaker(@PathVariable int id) {
        return speakerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid speaker id %s", id)));
    }

    @PostMapping(value = "/speaker", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Speaker postSpeaker(@Valid @RequestBody Speaker speaker) {
        return speakerRepository.save(speaker);
    }

}