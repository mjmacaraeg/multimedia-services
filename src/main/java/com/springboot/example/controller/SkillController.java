package com.springboot.example.controller;


import com.springboot.example.entity.Skill;
import com.springboot.example.repository.SkillRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class SkillController {
    private final SkillRepository skillRepository;

    public SkillController(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @GetMapping(value = "/skill", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Skill> getSkills() {
        return skillRepository.findAll();
    }

    @GetMapping(value = "skill/{skill_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Skill getSkill(@PathVariable long skill_id) {
        return skillRepository.findById(skill_id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid Skill id %s", skill_id)));
    }

    @PostMapping(value = "/skill", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Skill postSkill(@Valid @RequestBody Skill skill) {
        return skillRepository.save(skill);
    }

}