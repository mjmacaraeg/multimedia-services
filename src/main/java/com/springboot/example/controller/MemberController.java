package com.springboot.example.controller;


import com.springboot.example.entity.Member;
import com.springboot.example.repository.MemberRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MemberController {
    private final MemberRepository memberRepository;

    public MemberController(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @GetMapping(value = "/member", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Member> getMembers() {
        return memberRepository.findAll();
    }

    @GetMapping(value = "member/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Member getMember(@PathVariable int id) {
        return memberRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid Member id %s", id)));
    }

    @PostMapping(value = "/member", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Member postMember(@Valid @RequestBody Member member) {
        return memberRepository.save(member);
    }

}